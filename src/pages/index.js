import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
          <div className='text-center mt-10 bg-amber-100'>
              <div className='text-2xl mb-5'>나의 성격과 어울리는 고양이</div>
              <div className='text-5xl'>나와 어울리는 <strong className='text-blue-200'><strong>고양이</strong></strong>는?
              </div>
              <div className='flex justify-center mt-10'><img src="/cat1.png" alt="목도리한 서있는 고양이"/></div>
              <button className='bg-white border-4 p-6 px-40 my-20 font-bold'
                      onClick={() => router.push('/list')}>START
              </button>
          </div>
  );
}
