const BaseLayout = ({ children }) => {
    return (
        <>
            <body className='bg-amber-100'>{children}</body>
        </>
    )
}

export default BaseLayout